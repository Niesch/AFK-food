package main

type Order struct {
	Contents string
	Name     string
	Date     string
	Id       int
}

type OrderList struct {
	Orders    []Order
	currentID int
}

func (l *OrderList) Append(order Order) {
	l.Orders = append(l.Orders, order)
}

func (l *OrderList) DeleteFromID(id int) {
	var index int
	// find the index of the struct to delete
	for i, order := range l.Orders {
		if order.Id == id {
			index = i
		}
	}
	// remove index from Orders slice
	l.Orders = append(l.Orders[:index], l.Orders[index+1:]...)
}
